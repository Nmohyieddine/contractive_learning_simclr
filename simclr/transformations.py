from torchvision import transforms

def colorJitter(brightness=0.5,contrast=0.5,saturation=0.5,hue=0.1,p=0.8):
    return transforms.RandomApply([transforms.ColorJitter(brightness=brightness,contrast=contrast,saturation=saturation,hue=hue)], p=p)


def randomResizedCrop(size=96):
    return transforms.RandomResizedCrop(size=size)

def randomGrayscale(p=0.2):
    return transforms.RandomGrayscale(p=p)

def gaussianBlur(kernel_size=9):
    return transforms.GaussianBlur(kernel_size=kernel_size)



