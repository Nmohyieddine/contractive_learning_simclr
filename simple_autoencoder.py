__author__ = 'SherlockLiao'

import os

import torch

from torch import nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.datasets import CIFAR10
from torchvision.utils import save_image
from torch.utils.tensorboard import SummaryWriter


if not os.path.exists('./mlp_img'):
    os.mkdir('./mlp_img')


def to_img(x):
    x = 0.5 * (x + 1)
    x = x.clamp(0, 1)
    x = x.view(x.size(0), 3, 32, 32)
    return x


num_epochs = 100
batch_size = 128
learning_rate = 1e-3

img_transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize([0.5], [0.5])
])

dataset = CIFAR10('./data', transform=img_transform)
dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)


class autoencoder(nn.Module):
    def __init__(self):
        super(autoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(32 * 32*3, 128),
            nn.ReLU(True),
            nn.Linear(128, 64),
            nn.ReLU(True), nn.Linear(64, 12), nn.ReLU(True), nn.Linear(12, 3))
        self.decoder = nn.Sequential(
            nn.Linear(3, 12),
            nn.ReLU(True),
            nn.Linear(12, 64),
            nn.ReLU(True),
            nn.Linear(64, 128),
            nn.ReLU(True), nn.Linear(128, 32 * 32*3), nn.Tanh())

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

def tensorboardReader (writer,image,reconstructed,epoch,loss,device):
    image = to_img(image).to(device)
    print("image reshape",image.size())
    writer.add_images(f'images_batch {epoch}', image, epoch)
    reconstructed = to_img(reconstructed).to(device)
    writer.add_images(f'reconstructions_batch {epoch}', reconstructed, epoch)
    writer.add_scalar('train_loss', loss , epoch)


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = autoencoder().cuda()
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(
    model.parameters(), lr=learning_rate, weight_decay=1e-5)
writer = SummaryWriter()

for epoch in range(num_epochs):
    for data in dataloader:
        img, _ = data
        #img = img.view(img.size(0), -1)
        #print(img.size())
        #img = Variable(img).cuda()
        img = img.reshape(-1, 32 * 32*3).to(device)
        print(img.size())
        # ===================forward=====================
        output = model(img)
        #print(output.size())
        loss = criterion(output, img)
        # ===================backward====================
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    tensorboardReader(writer,img,output,epoch,loss.tolist(),device)
    # ===================log========================
    print(f"epoch [{epoch + 1}/{num_epochs}], loss:{loss.tolist()}")
    if epoch % 10 == 0:
        pic = to_img(output.cpu().data)
        save_image(pic, './mlp_img/image_{}.png'.format(epoch))
        #img = img.reshape(-1, 28 * 28).cuda()
        #output = output.reshape(-1, 28 * 28).cuda()
        #writer.add_images(f'images_batch {epoch}', img, epoch)
        #writer.add_images(f'output_batch {epoch}', output, epoch)

#torch.save(model.state_dict(), './sim_autoencoder.pth')
torch.save(model.encoder, 'AutoEncoder/models/newencoder.pt')
