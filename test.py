import torch
from simple_autoencoder import autoencoder
from torchvision.datasets import MNIST
from torch.utils.data import DataLoader
from torchvision import transforms
from torch.utils.tensorboard import SummaryWriter



num_epochs = 100
batch_size = 128
learning_rate = 1e-3

img_transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize([0.5], [0.5])
])

dataset = MNIST('./data',train=False, transform=img_transform)
dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

encoder = torch.load('AutoEncoder/models/newencoder.pt')
writer = SummaryWriter()
for i, (images, labels) in enumerate(dataloader):
    image = images.reshape(-1, 28 * 28).cuda()

    reconstruction = encoder(image)
    writer.add_images(f'images_batch {i}', image, i)
    writer.add_images(f'reconstructions_batch {i}', reconstruction, i)
    print("epoch",i)




