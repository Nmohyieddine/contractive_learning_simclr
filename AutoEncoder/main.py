import os
from copy import deepcopy

import torch
from torch.utils import data
from torchvision import datasets
from torchvision import transforms
import matplotlib

from AutoEncoder.AE import AE
from AutoEncoder.LogisticRegression import LogisticRegression

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from torch.utils.tensorboard import SummaryWriter
import sklearn.cluster as cluster
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import torch.utils.data

import pytorch_lightning as pl
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint
from tqdm import tqdm
## Standard libraries
import os
from copy import deepcopy

## Imports for plotting
import matplotlib.pyplot as plt
plt.set_cmap('cividis')
from IPython.display import set_matplotlib_formats
set_matplotlib_formats('svg', 'pdf') # For export
import matplotlib
matplotlib.rcParams['lines.linewidth'] = 2.0
import seaborn as sns
sns.set()

## tqdm for loading bars
from tqdm.notebook import tqdm

## PyTorch
import torch
import torch.utils.data as data





CHECKPOINT_PATH = "../saved_models/tutorial17"
NUM_WORKERS = os.cpu_count()




CHECKPOINT_PATH = "../saved_models/tutorial17"

def prepareMnist(size):
    # Transforms images to a PyTorch Tensor
    tensor_transform = transforms.Compose([
        transforms.ToTensor()
        #transforms.Normalize([0.5], [0.5], [0.5])
    ])

    # Download the MNIST Dataset
    data_train = datasets.CIFAR10(root = "./data",
                             train = True,
                             download = True,
                             transform = tensor_transform)

    # DataLoader is used to load the dataset
    # for training
    loader_train = torch.utils.data.DataLoader(dataset = data_train,
                                         batch_size =int(size),
                                         shuffle = False)

    data_test = datasets.CIFAR10(root="./data",
                                  train=False,
                                  download=True,
                                  transform=tensor_transform)

    # DataLoader is used to load the dataset
    # for training
    loader_test = torch.utils.data.DataLoader(dataset=data_test,
                                               batch_size=int(size),
                                               shuffle=False)
    return loader_train, loader_test, data_train,data_test



def train(epochs, model,loader,loss_function,optimizer,device,batch_size):
    writer = SummaryWriter()
    losses = []
    outputs = []
    for epoch in range(int(epochs)):
        running_loss = 0
        train_loss = 0
        for i, (images, _) in enumerate(loader):
            # Reshaping the image to (-1, 784)
            print("images", images.size())
            image = images.reshape(-1, 32 * 32*3).to(device)
            # Output of Autoencoder
            print("image", image.size())
            reconstructed = model(image)
            print("reconstructed",reconstructed.size())

            # Calculating the loss function
            loss = loss_function(reconstructed, image)

            # The gradients are set to zero,
            # the gradient is computed and stored.
            # .step() performs parameter update
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            print("loss",loss.tolist())
            # Storing the losses in a list for plotting
            running_loss += loss.tolist()
            print(running_loss)
        train_loss = running_loss/len(loader)
        writer.add_scalar('train_loss', train_loss, epoch)
        outputs.append((epochs, image, reconstructed))
        tensorboardReader(writer, image, reconstructed, epoch, device,batch_size)


    return losses, outputs


def tensorboardReader (writer,image,reconstructed,epoch,device,batch_size):
    print(image.size())
    image = image.reshape(image.size(0), 3, 32, 32).to(device)
    writer.add_images(f'images_batch {epoch}', image, epoch)
    reconstructed = reconstructed.reshape(image.size(0), 3, 32, 32).to(device)
    writer.add_images(f'reconstructions_batch {epoch}', reconstructed, epoch)



def initAutoencoder (mode, batch_size, epochs,reduction):

    #  use gpu if available
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Model Initialization
    model = AE().to(device)


    # Validation using MSE Loss function
    loss_function = torch.nn.MSELoss()

    # Using an Adam Optimizer with lr = 0.1
    optimizer = torch.optim.Adam(model.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    loader_train, loader_test,data_train,data_test = prepareMnist(batch_size)

    if mode == "train":
        train(epochs, model, loader_train, loss_function, optimizer, device,batch_size)
        torch.save(model.encoder, 'AutoEncoder/models/encoder512-400epo.pt')
    elif mode == "predict":
        h, labels = predict(loader_test, device)
        flat_h = [item for sublist in h for item in sublist]
        flat_labels = [item for sublist in labels for item in sublist]
        print("flat_labels",flat_labels)
        flat_h_tostring = [str(item) for item in flat_h]
        with open(r'h_values_testloader.txt', 'w') as fp:
            fp.write('\n'.join(flat_h_tostring))

        df_init = pd.DataFrame(flat_h)

        df_copy = df_init.copy()
        df_copy.columns = df_copy.columns.astype(str)



        if reduction == "kmeans":
            kmeans = cluster.KMeans(n_clusters=10, init="k-means++")
            kmeans = kmeans.fit(flat_h)
            df_copy['Clusters'] = kmeans.labels_
            print(df_copy)
            print(df_copy['Clusters'].value_counts())
            y = df_copy['Clusters']

            dict_colors = {0: "forestgreen", 1: "royalblue", 2: "orange", 3: "grey", 4: "black", 5: "yellow",
                           6: "black", 7: "pink", 8: "blue", 9: "silver"}
            y_colors = [dict_colors[yi] for yi in y]
            df_copy.plot(x='PC1', y='PC2', kind='scatter', figsize=(5, 5), color=y_colors)
            plt.show()


        if reduction == "pca":
            pca(df_copy, flat_labels,2)
        if reduction == "t-sne":
            t_sne(df_init)
        if reduction == "real":
            real(df_copy, flat_labels)
        if reduction == "lr":
            encoder = torch.load('AutoEncoder/models/encoder512-400epo.pt', map_location=torch.device('cpu'))

            encoder = encoder.to(device)
            results = {}
            train_feats_simclr = prepare_data_features(encoder, data_train, device)
            test_feats_simclr = prepare_data_features(encoder, data_test, device)
            for num_imgs_per_label in [10, 20, 50, 100, 200, 500]:
                sub_train_set = get_smaller_dataset(train_feats_simclr, num_imgs_per_label)
                _, small_set_results = train_logreg(batch_size=64,
                                                    train_feats_data=sub_train_set,
                                                    test_feats_data=test_feats_simclr,
                                                    model_suffix=num_imgs_per_label,
                                                    device=device,
                                                    feature_dim=train_feats_simclr.tensors[0].shape[1],
                                                    num_classes=10,
                                                    lr=1e-3,
                                                    weight_decay=1e-3)
                results[num_imgs_per_label] = small_set_results



def real(df,labels):
    scaler = StandardScaler()
    scaler.fit(df)
    scaled_data = scaler.transform(df)
    pca = PCA(n_components=2)
    pca.fit(scaled_data)
    x_pca = pca.transform(scaled_data)
    # plot resutls
    pca_columns = ['PC' + str(c) for c in range(1, x_pca.shape[1] + 1, 1)]
    X_pca = pd.DataFrame(x_pca, index=df.select_dtypes('number').index, columns=pca_columns)  # création du dataframe
    print(X_pca)

    #kmeans = cluster.KMeans(n_clusters=10, init="k-means++")
    #kmeans = kmeans.fit(X_pca[['PC1', 'PC2']])
    X_pca['Clusters'] = labels
    print(X_pca)
    print(X_pca['Clusters'].value_counts())
    y = X_pca['Clusters']
    print(y)
    dict_colors = {0: "forestgreen", 1: "royalblue", 2: "orange", 3: "grey", 4: "black", 5: "yellow", 6: "black",
                   7: "pink", 8: "blue", 9: "silver"}
    y_colors = [dict_colors[yi] for yi in y]
    print(y_colors)
    X_pca.plot(x='PC1', y='PC2', kind='scatter', figsize=(5, 5), color=y_colors)
    plt.show()


def pca(df,labels,dimension):
    scaler = StandardScaler()
    scaler.fit(df)
    scaled_data = scaler.transform(df)

    pca = PCA(n_components=3)
    pca.fit(scaled_data)
    x_pca = pca.transform(scaled_data)

    # plot resutls
    pca_columns = ['PC' + str(c) for c in range(1, x_pca.shape[1] + 1, 1)]
    X_pca = pd.DataFrame(x_pca, index=df.select_dtypes('number').index, columns=pca_columns)  # création du dataframe
    print("Data test ")
    print(X_pca)
    X_pca_real = X_pca.copy()

    kmeans = cluster.KMeans(n_clusters=10, init="k-means++")
    if dimension == 2:
        kmeans = kmeans.fit(X_pca[['PC1', 'PC2']])
    if dimension == 3:
        kmeans = kmeans.fit(X_pca[['PC1', 'PC2', 'PC3']])
    X_pca['Clusters'] = kmeans.labels_
    X_pca_real['Clusters'] = labels
    print("with kmean labels")
    print(X_pca)
    print("with real labels")
    print(X_pca_real)
    print(X_pca['Clusters'].value_counts())
    print(X_pca_real['Clusters'].value_counts())
    ## majority
    Label0_list, Label1_list, Label2_list, Label3_list, Label4_list, Label5_list, Label6_list, Label7_list, Label8_list, Label9_list, = [], [], [], [], [], [], [], [], [], []
    nbr_elements = 1/1000
    for c in range(10):
        index = X_pca.index[X_pca['Clusters'] == c].tolist()
        if c == 0:
            print("cluster1")
            print(index)
        if c == 5:
            print("cluser5")
            print(index)
        labels_list = []

        for i in index:
            labels_list.append(labels[i])

            label0, label1, label2, label3, label4, label5, label6, label7, label8, label9 = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

            for l in labels_list:
                if l == 0:
                    label0 += 1
                elif l == 1:
                    label1 += 1
                elif l == 2:
                    label2 += 1
                elif l == 3:
                    label3 += 1
                elif l == 4:
                    label4 += 1
                elif l == 5:
                    label5 += 1
                elif l == 6:
                    label6 += 1
                elif l == 7:
                    label7 += 1
                elif l == 8:
                    label8 += 1
                else:
                    label9 += 1

        Label0_list.append(label0*nbr_elements)
        Label1_list.append(label1*nbr_elements)
        Label2_list.append(label2*nbr_elements)
        Label3_list.append(label3*nbr_elements)
        Label4_list.append(label4*nbr_elements)
        Label5_list.append(label5*nbr_elements)
        Label6_list.append(label6*nbr_elements)
        Label7_list.append(label7*nbr_elements)
        Label8_list.append(label8*nbr_elements)
        Label9_list.append(label9*nbr_elements)

    # , 'dominant label': dominant_label_list
    d = {'0': Label0_list, '1': Label1_list, '2': Label2_list, '3': Label3_list,'4': Label4_list, '5': Label5_list, '6': Label6_list, '7': Label7_list,'8': Label8_list, '9': Label9_list}
    df = pd.DataFrame(data=d, index=['0', '1', '2', '3', '4', '5', '6','7', '8', '9', ])
    df['maxval'] = df[['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']].max(axis=1)
    df['dominant_label'] = df.loc[:, :'9'].eq(df['maxval'], axis=0).apply(lambda x: ','.join(df.columns[:10][x == x.max()]), axis=1)

    print(df)


    #intersection

    inter = pd.DataFrame(data= df['dominant_label'], index=df.index)

    print("inter clusters kmeans and labels")
    print(inter)

    change_list = inter['dominant_label'].tolist()

    for clu in range(10):
        X_pca['Clusters'] = X_pca['Clusters'].replace(clu, int(change_list[clu]))

    print(X_pca)
    if dimension == 2:
        inter_stat = pd.merge(X_pca,X_pca_real, how='inner', on=['Clusters', 'PC1', 'PC2'])
    if dimension == 3:
        inter_stat = pd.merge(X_pca,X_pca_real, how='inner', on=['Clusters', 'PC1', 'PC2', 'PC3'])
    #percentage
    stat = inter_stat['Clusters'].value_counts().to_frame()

    stat['percentage'] = stat.apply(lambda row: row['Clusters'] * 0.1, axis=1)
    print('###stat###')
    print(stat)
    stat.plot(x="Clusters", y="percentage", kind="bar")

    y = X_pca['Clusters']
    y_real = X_pca_real['Clusters']
    y_inter = inter_stat['Clusters']
    inter_plot = pd.DataFrame(y_inter)


    print(inter)


    dict_colors = {0: "forestgreen", 1: "royalblue", 2: "orange", 3: "grey", 4: "black", 5: "yellow", 6: "black", 7: "pink", 8: "blue", 9: "silver"}
    dict_class0 = {0: "forestgreen", 1: None, 2: None , 3: None , 4: None , 5: None , 6: None , 7: None , 8: None , 9: None }
    y_colors = [dict_colors[yi] for yi in y]
    y_colors_real = [dict_colors[yi] for yi in y_real]
    y_inter = [dict_colors[yi] for yi in y_inter]
    y_colors_class0 = [dict_class0[yi] for yi in y_real]

    ax = X_pca.plot(x='PC1', y='PC2', kind='scatter', figsize=(5, 5), color=y_colors)
    X_pca_real.plot(ax=ax, x='PC1', y='PC2', kind='scatter', figsize=(5, 5), color=y_colors_class0)
    plt.show()

    #plot 3D
    #fig = plt.figure(figsize=(5, 5))
    #ax = fig.add_subplot(projection='3d')
    #ax.scatter(X_pca['PC1'], X_pca['PC2'], X_pca['PC3'], marker='o', s=30, edgecolor='k', facecolor=y_colors)
    #ax.scatter(inter_stat['PC1'], inter_stat['PC2'], inter_stat['PC3'], marker='x', s=30, edgecolor='k', facecolor=y_inter)
    #ax.set_xlabel('PC1 - ' )
    #ax.set_ylabel('PC2 - ')
    #ax.set_zlabel('PC3 - ' )
    #ax.view_init(elev=15, azim=45)
    #plt.show()


def t_sne(df):
    scaler = StandardScaler()
    scaler.fit(df)
    scaled_data = scaler.transform(df)
    tsne = TSNE(n_components=2, init='pca', random_state=0, n_jobs=-1)
    x_tsne = tsne.fit_transform(scaled_data)
    columns = ['DIM' + str(c) for c in range(1, x_tsne.shape[1] + 1, 1)]
    x_tsne = pd.DataFrame(x_tsne, index=df.select_dtypes('number').index, columns=columns)
    print(x_tsne)

    kmeans = cluster.KMeans(n_clusters=10, init="k-means++")
    kmeans = kmeans.fit(x_tsne[['DIM1', 'DIM2']])
    x_tsne['Clusters'] = kmeans.labels_
    print(x_tsne)
    print(x_tsne['Clusters'].value_counts())
    y = x_tsne['Clusters']
    print(y)
    dict_colors = {0: "forestgreen", 1: "royalblue", 2: "orange", 3: "grey", 4: "black", 5: "yellow", 6: "black",
                   7: "pink", 8: "blue", 9: "silver"}
    y_colors = [dict_colors[yi] for yi in y]
    print(y_colors)
    x_tsne.plot(x='DIM1', y='DIM2', kind='scatter', figsize=(5, 5), color=y_colors)
    plt.show()





def predict(loader, device):
    encoder = torch.load('AutoEncoder/models/encoder512-400epo.pt', map_location=torch.device('cpu'))

    encoder = encoder.to(device)
    h = []
    lab = []
    for i, (images, labels) in enumerate(loader):

        image = images.reshape(-1, 32 * 32*3).to(device)

        h_vec = encoder(image)
        h.append(h_vec.tolist())
        lab.append(labels.tolist())

    return h, lab



def prepare_data_features(model, dataset,device):
    # Prepare model
    network = deepcopy(model)
    network.eval()
    network.to(device)

    # Encode all images
    data_loader = data.DataLoader(dataset, batch_size=128, num_workers=NUM_WORKERS, shuffle=False, drop_last=False)
    feats, labels = [], []
    for batch_imgs, batch_labels in tqdm(data_loader):
        batch_imgs = batch_imgs.reshape(-1, 32 * 32 * 3).to(device)
        #image = images.reshape(-1, 32 * 32 * 3).to(device)

        batch_imgs = batch_imgs.to(device)
        batch_feats = network(batch_imgs)
        feats.append(batch_feats.detach().cpu())
        labels.append(batch_labels)

    feats = torch.cat(feats, dim=0)
    labels = torch.cat(labels, dim=0)

    # Sort images by labels
    labels, idxs = labels.sort()
    feats = feats[idxs]

    return data.TensorDataset(feats, labels)

def train_logreg(batch_size, train_feats_data, test_feats_data, model_suffix,device, max_epochs=100, **kwargs):
    trainer = pl.Trainer(default_root_dir=os.path.join(CHECKPOINT_PATH, "LogisticRegression"),
                         accelerator="gpu" if str(device).startswith("cuda") else "cpu",
                         devices=1,
                         max_epochs=max_epochs,
                         callbacks=[ModelCheckpoint(save_weights_only=True, mode='max', monitor='val_acc'),
                                    LearningRateMonitor("epoch")],
                         enable_progress_bar=False,
                         check_val_every_n_epoch=10)
    trainer.logger._default_hp_metric = None

    # Data loaders
    train_loader = data.DataLoader(train_feats_data, batch_size=batch_size, shuffle=True,
                                   drop_last=False, pin_memory=True, num_workers=0)
    test_loader = data.DataLoader(test_feats_data, batch_size=batch_size, shuffle=False,
                                  drop_last=False, pin_memory=True, num_workers=0)

    # Check whether pretrained model exists. If yes, load it and skip training
    pretrained_filename = os.path.join(CHECKPOINT_PATH, f"LogisticRegression_{model_suffix}.ckpt")
    if os.path.isfile(pretrained_filename):
        print(f"Found pretrained model at {pretrained_filename}, loading...")
        model = LogisticRegression.load_from_checkpoint(pretrained_filename)
    else:
        pl.seed_everything(42)  # To be reproducable
        model = LogisticRegression(**kwargs)
        trainer.fit(model, train_loader, test_loader)
        model = LogisticRegression.load_from_checkpoint(trainer.checkpoint_callback.best_model_path)

    # Test best model on train and validation set
    train_result = trainer.test(model, train_loader, verbose=False)
    test_result = trainer.test(model, test_loader, verbose=False)
    result = {"train": train_result[0]["test_acc"], "test": test_result[0]["test_acc"]}

    return model, result

def get_smaller_dataset(original_dataset, num_imgs_per_label):
    new_dataset = data.TensorDataset(
        *[t.unflatten(0, (10, -1))[:,:num_imgs_per_label].flatten(0, 1) for t in original_dataset.tensors]
    )
    return new_dataset



