import sys
import getopt

from AutoEncoder.main import initAutoencoder

def myfunc(argv):
    arg_model = ""
    arg_mode = ""
    arg_train_epochs = ""
    batch_size = ""
    arg_reduction = ""
    arg_help = "{0} -ml <model> -m <mode> -o <train_epochs> -s <batch_size>".format(argv[0])

    try:
        opts, args = getopt.getopt(argv[1:], "hm:d:r:e,s", ["help", "model=",
                                                         "mode=", "reduction=", "train_epochs=", "batch_size="])
    except:
        print(arg_help)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print(arg_help)  # print the help message
            sys.exit(2)
        elif opt in ("-ml", "--model"):
            arg_model = arg
        elif opt in ("-m", "--mode"):
            arg_mode = arg
        elif opt in ("-r", "--reduction"):
            arg_reduction = arg
        elif opt in ("-e", "--train_epochs"):
            arg_train_epochs = arg
        elif opt in ("-s", "--batch_size"):
            batch_size = arg

    print('model:', arg_model)
    print('mode:', arg_mode)
    print('reduction', arg_reduction)
    print('train_epochs:', arg_train_epochs)
    print('batch_size:', batch_size)

    return arg_model, arg_mode, arg_train_epochs, batch_size,arg_reduction


if __name__ == "__main__":
    arg_model, arg_mode, arg_train_epochs, arg_batch_size, arg_reduction = myfunc(sys.argv)
    if arg_model == "encoder":
        initAutoencoder(arg_mode, arg_batch_size, arg_train_epochs, arg_reduction)



